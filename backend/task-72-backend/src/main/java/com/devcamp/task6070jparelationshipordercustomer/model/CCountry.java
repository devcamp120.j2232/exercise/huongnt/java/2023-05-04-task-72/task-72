package com.devcamp.task6070jparelationshipordercustomer.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "country")
public class CCountry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "country_code", unique = true)
    private String countryCode;

    @Column(name = "country_name")
    private String countryName;

    @OneToMany(mappedBy = "country", cascade = CascadeType.ALL, fetch = FetchType.LAZY)//giống targetEntity = CRegion.class
    private List<CRegion> regions;

    
    public CCountry() {
    }

    public CCountry(Long id, String countryCode, String countryName, List<CRegion> regions) {
        this.id = id;
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.regions = regions;
    }

    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public List<CRegion> getRegions() {
		return regions;
	}

	public void setRegions(List<CRegion> regions) {
		this.regions = regions;
	}
}

