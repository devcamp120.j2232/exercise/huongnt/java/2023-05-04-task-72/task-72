package com.devcamp.task6070jparelationshipordercustomer.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
//import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task6070jparelationshipordercustomer.model.CCountry;
import com.devcamp.task6070jparelationshipordercustomer.repository.CountryRepository;

@RestController
@CrossOrigin
@RequestMapping("/")

public class CountryController {
	@Autowired
	private CountryRepository countryRepository;

	// get all
	@GetMapping("/countries")
	public ResponseEntity<List<CCountry>> getAllCountries() {
		try {
			List<CCountry> countryList = countryRepository.findAll();
			return new ResponseEntity<>(countryList, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// create
	@PostMapping("/country/create")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			CCountry newRole = new CCountry();
			newRole.setCountryName(cCountry.getCountryName());
			newRole.setCountryCode(cCountry.getCountryCode());
			newRole.setRegions(cCountry.getRegions());
			CCountry savedRole = countryRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
		}
	}

	// update
	@PutMapping("/country/update/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CCountry cCountry) {
		Optional<CCountry> countryData = countryRepository.findById(id);
		if (countryData.isPresent()) {
			CCountry newCountry = countryData.get();
			newCountry.setCountryName(cCountry.getCountryName());
			newCountry.setCountryCode(cCountry.getCountryCode());
			newCountry.setRegions(cCountry.getRegions());
			CCountry savedCountry = countryRepository.save(newCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	// delete
	@DeleteMapping("/country/delete/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			Optional<CCountry> optional = countryRepository.findById(id);
			if (optional.isPresent()) {
				countryRepository.deleteById(id);
			} else {
				// countryRepository.deleteById(id);
			}
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/country/detail/{id}")
	public CCountry getCountryById(@PathVariable Long id) {
		if (countryRepository.findById(id).isPresent())
			return countryRepository.findById(id).get();
		else
			return null;
	}

	@CrossOrigin
	@GetMapping("/country/all")
	public List<CCountry> getAllCountry() {
		return countryRepository.findAll();
	}

	@GetMapping("/countries-count")
	public long countCountry() {
		return countryRepository.count();
	}

	@GetMapping("/countries/check/{id}")
	public boolean checkCountryById(@PathVariable Long id) {
		return countryRepository.existsById(id);
	}

	@GetMapping("/country/code/{code}")
	public CCountry getCountryByCode(@PathVariable String code) {
		return countryRepository.findByCountryCode(code);
	}

	@GetMapping("/country/containing-code/{code}")
	public CCountry getCountryByContainingCode(@PathVariable String code) {
		return countryRepository.findByCountryCodeContaining(code);
	}

	@GetMapping("/country/name/{name}")
	public CCountry getCountryByName(@PathVariable String name) {
		return countryRepository.findByCountryName(name);
	}

	// API để phân trang
	@CrossOrigin
	@GetMapping("/country5")
	public ResponseEntity<List<CCountry>> getCountryByPage(
			@RequestParam(value = "page", defaultValue = "1") String page,
			@RequestParam(value = "size", defaultValue = "5") String size) {
		try {
			Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
			List<CCountry> list = new ArrayList<CCountry>();
			countryRepository.findAll(pageWithFiveElements).forEach(list::add);
			return new ResponseEntity<>(list, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
