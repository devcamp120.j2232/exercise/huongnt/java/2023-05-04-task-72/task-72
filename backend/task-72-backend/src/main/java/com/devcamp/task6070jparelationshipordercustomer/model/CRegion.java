package com.devcamp.task6070jparelationshipordercustomer.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(name = "region")
public class CRegion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(unique = true,name = "region_code")
    private String regionCode;
    
    @Column(name = "region_name")
    private String regionName;

    @ManyToOne (fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "country_id", nullable = false)
    private CCountry country;
    
    @Transient //không hiển thị trong DB
    private String countryName; //thêm thuộc tính riêng để sử dụng trưc tiếp trong một số trường hợp
    @JsonIgnore
    public String getCountryName() {
        return getCountry().getCountryName();
    }
    
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public CRegion() {
    }

    public CRegion(Long id, String regionCode, String regionName) {
        this.id = id;
        this.regionCode = regionCode;
        this.regionName = regionName;
    }

    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getRegionName() {
        return regionName;
    }
    
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionCode() {
        return regionCode;
    }
    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }
    @JsonIgnore
    public CCountry getCountry() {
        return country;
    }

    public void setCountry(CCountry cCountry) {
        this.country = cCountry;
    }
}

