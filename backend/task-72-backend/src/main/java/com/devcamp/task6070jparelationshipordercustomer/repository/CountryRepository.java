package com.devcamp.task6070jparelationshipordercustomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6070jparelationshipordercustomer.model.CCountry;

public interface CountryRepository extends JpaRepository<CCountry, Long>{
    CCountry findByCountryCode(String countryCode);
    CCountry findByCountryCodeContaining(String countryCode);
	CCountry findByCountryName(String countryName);

}


